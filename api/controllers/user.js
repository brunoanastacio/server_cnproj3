'use strict';
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config  = require('../../config/config.js');
const configAWS  = require('../../config/config_dynamo.json');
var multer  = require('multer'), multerS3 = require('multer-s3');
const fs = require('fs');
var path = require('path')
var AWS = require('aws-sdk');

var mongoosePaginate = require('mongoose-paginate');

const User = require("../models/user");

var s3 = new AWS.S3({
  accessKeyId: configAWS.accessKeyId,
  secretAccessKey: configAWS.secretAccessKey,
  region: configAWS.region
});

exports.all_users = (req, res, next) => {
  var userId = req.userData.userId;
  User.find({ }).select("-password").sort({name:1}).populate('friend_requests._id').populate('friends._id')
    .exec(function(error, users) {
      if (error){
        return res.status(401).json({
          message: "Error, get all users!"
        });
      }
      if(users){    
        User.findById(userId, function (err, user) {
          if (error){
            return res.status(401).json({
              message: "Error in get auth user!"
            });
          }
          if(user){
            var allUsers = JSON.stringify(users);
            return res.status(200).json({
              users: allUsers,
              _id: userId,
              authUser: user
            });
          }
        }); 
      }else{
        return res.status(401).json({
          message: "Error in get all users!"
        });
      }
    });
};

exports.all_users_v2 = (req, res, next) => {
  var userId = req.userData.userId;
  var page = req.params.page;

  console.log(page);

    var options = {
      select:   '-password',
      sort:     { name: 1 },
      populate: ['friend_requests._id','friends._id'],
      lean:     true,
      page: page,
      limit:    20
  };
  
  User.paginate({}, options).then(function(result) {
    User.findById(userId, function (err, user) {
      if (err){
        return res.status(401).json({
          message: "Error in get auth user!"
        });
      }
      if(user){
        return res.status(200).json({
          users: result,
          _id: userId,
          authUser: user
        });
      }
    });   
  });
};

exports.user_signup = (req, res, next) => {
  User.find({ username: req.body.username })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return res.status(409).json({
          message: "Username already exists"
        });
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            var img_photo = "https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/512x512/businessman.png"; //default image
            if(req.body.img_photo != ""){
              img_photo = req.body.img_photo;
            }
            var user = new User({
              _id: new mongoose.Types.ObjectId(),
              username: req.body.username,
              password: hash,
              name: req.body.name,
              img_photo: img_photo
            });
            user
              .save()
              .then(result => {
               // console.log(result);
                res.status(201).json({
                  message: "User created successfully"
                });
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
              });
          }
        });
      }
    });
};

exports.user_login = (req, res, next) => {
  User.find({ username: req.body.username })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: "Auth failed, user not exists"
        });
      }
      bcrypt.compare(req.body.password, user[0].password, (err, result) => {
        if (err) {
          return res.status(401).json({
            message: "Auth failed, invalid password"
          });
        }
        if (result) {
          const token = jwt.sign(
            {
              username: user[0].username,
              userId: user[0]._id
            },
            config.jwt_key,
            {
              expiresIn: "1h"
            }
          );
          return res.status(200).json({
            message: "Auth successful",
            token: token,
            userId: user[0]._id,
            user: user
          });
        }
        res.status(401).json({
          message: "Auth failed"
        });
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.user_delete = (req, res, next) => {
  var userId = req.userData.userId;
  User.remove({ _id: userId })
    .exec()
    .then(result => {
      res.status(200).json({
        message: "User deleted successfully"
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.user_logout = (req, res, next) => {
  var userId = req.userData.userId;
  User.find({ _id: userId })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: "User not exists!"
        });
      }else{
        if(req.body.logout == true){
          return res.status(200).json({
            message: "Logout successful"
          });
        }
        return res.status(200).json({
          message: "Logout fail"
        });
      }
    })
    .catch(err => {
     // console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.user_change_password = (req, res, next) => {
  User.findOne({ username: req.params.username })
    .exec()
    .then(user => {
      if (!user) {
        return res.status(401).json({
          message: "User not exists!"
        });
      }else{
        bcrypt.compare(req.body.oldpassword, user.password, (err, result) => {
          if (err) {
            return res.status(401).json({
              message: "Old password is wrong!"
            });
          }
          if (result) {

            bcrypt.hash(req.body.password, 10, (err, hash) => {
              if (err) {
                return res.status(500).json({
                  error: err
                });
              } else {
                user.password = hash;
                user.save(function (err, data) {
                  if (err){
                    return res.status(401).json({
                      message: "Change password failed!"
                    });
                  }
                  if(data){
                    return res.status(200).json({
                      message: "Change password succesfully",
                      user: data
                    });
                  }
                }); 
              }
            });
          }
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.make_friend_request = (req, res, next) => {
  console.log("friend_request");
  var userId = req.userData.userId;
  var friend_request_username = req.params.username;

  User.findOne({ username: friend_request_username })
    .exec()
    .then(friend_request => {
      if (!friend_request) {
        return res.status(401).json({
          message: "User not exists!"
        });
      }else{
        var aux = false;
        for (var j = 0; j < friend_request.friend_requests.length; j++){
          if(friend_request.friend_requests[j]._id == userId){
            aux = true;
          }
        }  
        if(!aux){
          friend_request.friend_requests.push(userId);
          friend_request.save(function (err, data) {
            if (err){
              return res.status(401).json({
                message: "Friend request not saved!"
              });
            }
            if(data){
              //console.log("data: "); console.log(data);
              return res.status(200).json({
                message: "Friend request saved!",
                user: data
              });
            }
          }); 
        }else{
          return res.status(200).json({
            message: "Friend request already exists on DB!"
          });
        } 
    
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.get_friend_requests = (req, res, next) => {
  var userId = req.userData.userId;

  User.findOne({ _id: userId }).select("-password")
    .populate('friend_requests._id')
    .exec(function(error, friend_requests) {
      if (error){
        return res.status(401).json({
          message: "Error, get all friend requests failed!"
        });
      }
      if(friend_requests){
       // console.log(JSON.stringify(friend_requests, null, "\t"))
        var friend_requests = JSON.stringify(friend_requests);
        //console.log(friend_requests);
        return res.status(200).json({
          message: "All friend requests!",
          friend_requests : friend_requests
        });
      }
    });
};

exports.accept_friend_request = (req, res, next) => {
  console.log("accept friend_request");
  var userId = req.userData.userId;
  var friend_request_username = req.params.username;

  User.findOne({ _id: userId })
  .exec()
  .then(user => {
    if (!user) {
      return res.status(401).json({
        message: "User not exists!"
      });
    }else{
      User.findOne({ username: friend_request_username })
        .exec()
        .then(friend_request => {
          if (!friend_request) {
            return res.status(401).json({
              message: "User not exists!"
            });
          }else{
            for(var i = 0; i < user.friend_requests.length; i++) {
              if(user.friend_requests[i]._id.toString() == friend_request._id.toString()) {
                user.friend_requests.splice(i, 1);
              }
            }

            var friendAlreadyExists = false;
           // console.log("FRIENDs ARRAY");
           // console.log(user.friends);
            for(var i = 0; i < user.friends.length; i++) {
              //console("loop"); console.log(user.friends[i]._id.toString() == friend_request._id.toString());
              if(user.friends[i]._id.toString() == friend_request._id.toString()) {
                friendAlreadyExists = true;
              }
            }
            if(friendAlreadyExists == false){
              user.friends.push(friend_request._id);
            }

            var friendExists = false;
            for(var i = 0; i < friend_request.friends.length; i++) {
              if(friend_request.friends[i]._id.toString() == user._id.toString()) {
                friendExists = true;
              }
            }
            if(friendExists == false){
              friend_request.friends.push(user._id);
            }

            user.save(function (err, data) {
              if (err){
                return res.status(401).json({
                  message: "Friend request not accepted!"
                });
              }
              if(data){
                friend_request.save(function (err, data) {
                  if (err){
                    return res.status(401).json({
                      message: "Friend request not accepted!"
                    });
                  }
                  if(data){
                    //console.log("data: "); console.log(data);
                    return res.status(200).json({
                      message: "Friend request accepted",
                      user: data
                    });
                  }
                }); 
              }

            }); 
          }
        });
    }
  })
};

exports.refuse_friend_request = (req, res, next) => {
  console.log("refuse friend_request");
  var userId = req.userData.userId;
  var friend_username = req.params.username;

  User.findOne({ _id: userId })
  .exec()
  .then(user => {
    if (!user) {
      return res.status(401).json({
        message: "User not exists!"
      });
    }else{
      User.findOne({ username: friend_username })
        .exec()
        .then(friend => {
          if (!friend) {
            return res.status(401).json({
              message: "User not exists!"
            });
          }else{
            user.friend_requests.id(friend._id).remove();
            user.save(function (err, data) {
              if (err){
                return res.status(401).json({
                  message: "Friend request not refused!"
                });
              }
              if(data){
                //console.log("data: "); console.log(data);
                return res.status(200).json({
                  message: "Friend requests refused succefully",
                  user: data
                });
              }
            }); 
          }
      });
    }
  })
};

exports.get_user = (req, res, next) => {
  var userId = req.userData.userId;
  var getUserId = req.params.userId;

  User.findOne({_id: getUserId }).select("-password").populate('friend_requests._id').populate('friends._id')
    .exec(function(error, user) {
      if (error){
        return res.status(401).json({
          message: "Error, get all users!"
        });
      }
      if(user){
        return res.status(200).json({
          message: "Success in get user!",
          user: user
        });
      }else{
        return res.status(401).json({
          message: "Error in get user!"
        });
      }
    });
};

exports.all_friends = (req, res, next) => {
  var userId = req.userData.userId;

  User.findOne({ _id: userId }).select("-password")
    .populate('friends._id')
    .exec(function(error, friends) {
      if (error){
        return res.status(401).json({
          message: "Error, get all friends failed!"
        });
      }
      if(friends){
       // console.log(JSON.stringify(friend_requests, null, "\t"))
        var friends = JSON.stringify(friends);
        //console.log(friends);
        return res.status(200).json({
          message: "All friends!",
          friends : friends
        });
      }
    });
};


exports.remove_friend = (req, res, next) => {
  console.log("remove friend");
  var userId = req.userData.userId;
  var friend_username = req.params.username;
  
  User.findOne({ _id: userId })
  .exec()
  .then(user => {
    if (!user) {
      return res.status(401).json({
        message: "User not exists!"
      });
    }else{
      User.findOne({ username: friend_username })
        .exec()
        .then(friend => {
          if (!friend) {
            return res.status(401).json({
              message: "User not exists!"
            });
          }else{
            // ir ao array de amigos do autenticado e remover amigo, ir ao array de amigos do amigo e remover-me
            user.friends.id(friend._id).remove();
            friend.friends.id(user._id).remove();

            user.save(function (err, data) {
              if (err){
                return res.status(401).json({
                  message: "Friend request not refused!"
                });
              }
              if(data){
                  friend.save(function (err, data) {
                    if (err){
                      return res.status(401).json({
                        message: "Friend request not refused!"
                      });
                    }
                    if(data){
                     // console.log("data: "); console.log(data);
                      return res.status(200).json({
                        message: "Friend requests refused succefully",
                        user: data
                      });
                    }
                  }); 
              }
            }); 
          }
      });
    }
  })
};

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

exports.user_update = (req, res, next) => {
  User.findOne({ username: req.params.username })
    .exec()
    .then(user => {
      if (!user) {
        return res.status(401).json({
          message: "User not exists!"
        });
      }else{
        if(req.body.username != ""){
          user.username = req.body.username;
        }
        if(req.body.name != ""){
          user.name = req.body.name;
        }
        if(req.body.msgimg != ""){
          if(req.file){
            user.img_photo = req.file.location;
          }else{
            user.img_photo = "https://d1nhio0ox7pgb.cloudfront.net/_img/g_collection_png/standard/512x512/businessman.png";
          }
        }
        user.save(function (err, data) {
          if (err){
            return res.status(401).json({
              message: "Change user failed!"
            });
          }
          if(data){
            return res.status(200).json({
              message: "Change user succesfully",
              user: data
            });
          }
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};