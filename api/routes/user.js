const express = require("express");
const router = express.Router();

const UserController = require('../controllers/user');
const checkAuth = require('../middleware/check-auth');
const configAWS  = require('../../config/config_dynamo.json');
var AWS = require('aws-sdk');
var path = require('path')
var multer  = require('multer'), multerS3 = require('multer-s3');

var s3 = new AWS.S3({
    accessKeyId: configAWS.accessKeyId,
    secretAccessKey: configAWS.secretAccessKey,
    region: configAWS.region
  });

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
};

//file.originalname
const uploadUserPhoto = multer({
    storage: multerS3({
          s3: s3,
          bucket: 'cnproj3',
          acl: 'public-read',
          metadata: function (req, file, cb) {
              cb(null, { fieldName: file.fieldname });
              if(file.originalname != undefined ){
                cb(null, { fieldName: file.fieldname });
              }
          },
          key: function (req, file, cb) {
              if(file.originalname != undefined ){
                var ext = path.extname(file.originalname);
                console.log(ext);
                cb(null, req.userData.userId+ext); //use Date.now() for unique file keys
              }
          }
      }),
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });


router.post("/update/:username", checkAuth, uploadUserPhoto.single('msgimg'), UserController.user_update);
router.get("/friend_requests",checkAuth, UserController.get_friend_requests);
router.get("/friends",checkAuth,UserController.all_friends);

router.get("/all", checkAuth, UserController.all_users);
router.get("/all_2/:page", checkAuth, UserController.all_users_v2);
router.get("/:userId",checkAuth,UserController.get_user);

router.post("/signup", UserController.user_signup);
router.post("/login", UserController.user_login);
router.delete("/:userId", checkAuth, UserController.user_delete);
router.post("/logout/:username", checkAuth, UserController.user_logout);
router.post("/changepw/:username", checkAuth, UserController.user_change_password);

//router.post("/update/:username", checkAuth, UserController.user_update);

router.post("/friend_request/:username",checkAuth, UserController.make_friend_request);

router.post("/friend_request/accept/:username",checkAuth,UserController.accept_friend_request);
router.post("/friend_request/refuse/:username",checkAuth,UserController.refuse_friend_request);

router.post("/friend/remove/:username",checkAuth,UserController.remove_friend);

module.exports = router;
