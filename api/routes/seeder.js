const express = require("express");
const router = express.Router();

const Seeder = require('../controllers/seeder');
/*
seeder.createUsers();

seeder.friendsFor3Firsts();*/


router.get("/create_users", Seeder.createUsers);
router.get("/get_user_ids", Seeder.saveUserIdsInArray);
router.get("/friend_first", Seeder.friendsFor3Firsts);
router.get("/friends_for_all", Seeder.friendsForAll);

router.get("/create_messages", Seeder.create_messages);

router.get("/ip", Seeder.get_ip);

module.exports = router;
