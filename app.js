//import DynamoHelper from './dynamo/helper.js';
var DynamoHelper = require('./dynamo/helper.js');
var config = require('./config/config.js');
var mongodb = require('mongodb').MongoClient;

var dynamo_helper = DynamoHelper.DynamoHelper;

const express = require("express");
const app = express();
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const userRoutes = require('./api/routes/user');
const messageRoutes = require('./api/routes/message');
const seederRoutes  = require('./api/routes/seeder.js');

//var a = uuid();
//console.log(a);
//dynamo_helper.listTables();
//dynamo_helper.createAllTables();
//table = dynamoDB.table('UsersPaaS');
//dynamo_helper.listTables();

mongoose.connect(config.db.uri);
mongoose.Promise = global.Promise;



app.use(morgan("dev"));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

app.use("/user", userRoutes);
app.use("/msg", messageRoutes);
app.use("/seeder", seederRoutes);

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
  });
  
  app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
      error: {
        message: error.message
      }
    });
  });
  
  module.exports = app;