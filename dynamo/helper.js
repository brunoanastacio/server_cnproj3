"use strict";
exports.__esModule = true;
// Load the SDK and UUID
var AWS = require('aws-sdk');
var uuid = require('uuid');
var DynamoHelper = /** @class */ (function () {
    function DynamoHelper() {
        AWS.config.loadFromPath('./config/config_dynamo.json');
        this.dynamoDB = new AWS.DynamoDB();
    }
    
    DynamoHelper.prototype.createAllTables = function () {
        this.createUsersTable();
        console.log("created tables");
    };
    DynamoHelper.prototype.createUsersTable = function () {
        var params = {
            TableName: "UsersPaaS",
            KeySchema: [
                { AttributeName: "id", KeyType: "HASH" },
                { AttributeName: "username", KeyType: "RANGE" }
            ],
            AttributeDefinitions: [
                { AttributeName: "id", AttributeType: "N" },
                { AttributeName: "username", AttributeType: "S" }
            ],
            ProvisionedThroughput: {
                ReadCapacityUnits: 1,
                WriteCapacityUnits: 1
            }
        };
        this.dynamoDB.createTable(params, function (err, data) {
            if (err) {
                console.error("Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
            }
            else {
                console.log("Created table. Table description JSON:", JSON.stringify(data, null, 2));
            }
        });
    };
    DynamoHelper.prototype.listTables = function () {
        /* var params = {
             ExclusiveStartTableName: 'STRING_VALUE',
             Limit: 0
         };
         */
        this.dynamoDB.listTables("", function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
            }
            else {
                if (data['TableNames'].length == 0) {
                    console.log("Não existem tabelas");
                    return;
                }
                console.log(data['TableNames'].length); // successful response
            }
        });
    };
    return DynamoHelper;
}());
exports.DynamoHelper = DynamoHelper;
