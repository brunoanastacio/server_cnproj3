'use strict';
const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config  = require('../../config/config.js');
const configAWS  = require('../../config/config_dynamo.json');
var multer  = require('multer'), multerS3 = require('multer-s3');
const fs = require('fs');
var path = require('path')
var AWS = require('aws-sdk');

const User = require("../models/user");
const Message = require("../models/message");

var s3 = new AWS.S3({
  accessKeyId: configAWS.accessKeyId,
  secretAccessKey: configAWS.secretAccessKey,
  region: configAWS.region
});


exports.createMsg = (req, res, next) => {
  var userId = req.userData.userId;

  var img = "";
  if(req.file){
    img = req.file.location;
  }
  
  var msg = new Message({
    _id: new mongoose.Types.ObjectId(),
    postedBy: userId,
    img_photo: img,
    message: req.body.msgtext,
    name: req.body.nameSender
  });

  msg.save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Message created successfully"
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};

exports.lastmsg = (req, res, next) => {
  var userId = req.userData.userId;
  User.findOne({ _id: userId })
  .exec()
  .then(user => {
    if (!user) {
      return res.status(401).json({
        message: "User not exists!"
      });
    }else{
      // procurar mensagens de todos os amigos
      user.friends.push(user._id);
      Message.find({ })
      .where('postedBy').in(user.friends)
      .limit(20)
      .sort('-date')
      .populate('postedBy','-password')
      .exec()
      .then(messages => {
        if (!messages) {
          return res.status(401).json({
            message: "Messages not exists!"
          });
        }else{
          // procurar todos os amigos
          console.log(messages);
          return res.status(200).json({
            message: "Messages returned succesfully",
            messages: messages
          });
          
        }
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      }); 
 
    }
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  }); 
};

exports.search = (req, res, next) => {
  var userId = req.userData.userId;
  var usernameFilter = req.query.usernameFilter;
  var messageFilter = req.query.messageFilter;
  
  User.findOne({ _id: userId })
  .exec()
  .then(user => {
    if (!user) {
      return res.status(401).json({
        message: "User not exists!"
      });
    }else{
      // procurar mensagens de todos os amigos
      if(messageFilter != "" && usernameFilter != ""){
        user.friends.push(user._id);
        Message.find({ message: new RegExp(messageFilter.replace(/\s+/g,"\\s+"), "gi"),  name: new RegExp(usernameFilter.replace(/\s+/g,"\\s+"), "gi")}  )
        .where('postedBy').in(user.friends)
        .limit(100)
        .sort('-date')
        .populate('postedBy','-password')
        .exec()
        .then(messages => {
          if (!messages) {
            return res.status(401).json({
              message: "Messages not exists!"
            });
          }else{
            // procurar todos os amigos
            console.log(messages);
            return res.status(200).json({
              message: "Messages returned succesfully",
              messages: messages
            });
            
          }
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }); 
        return;
      }

      if(messageFilter != ""){
        console.log("message filter");
        user.friends.push(user._id);
        Message.find( { message:  new RegExp(messageFilter.replace(/\s+/g,"\\s+"), "gi") } )
        .where('postedBy').in(user.friends)
        .limit(100)
        .sort('-date')
        .populate('postedBy','-password')
        .exec()
        .then(messages => {
          if (!messages) {
            return res.status(401).json({
              message: "Messages not exists!"
            });
          }else{
            // procurar todos os amigos
            console.log(messages);
            return res.status(200).json({
              message: "Messages returned succesfully",
              messages: messages
            });
            
          }
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }); 
        return;
      }

      if(usernameFilter != ""){
        user.friends.push(user._id);
        Message.find( { name: new RegExp(usernameFilter.replace(/\s+/g,"\\s+"), "gi") })
        .where('postedBy').in(user.friends)
        .limit(100)
        .sort('-date')
        .populate('postedBy','-password')
        .exec()
        .then(messages => {
          if (!messages) {
            return res.status(401).json({
              message: "Messages not exists!"
            });
          }else{
            // procurar todos os amigos
            console.log(messages);
            return res.status(200).json({
              message: "Messages returned succesfully",
              messages: messages
            });
            
          }
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        }); 
        return;
      } 
    }
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  }); 
};

exports.searchUserMessages = (req, res, next) => {
  var userId = req.userData.userId;
  var friendId = req.params.friendId;
  
  console.log("ID FRIEND: " + friendId );
  console.log("ID: " + userId);

  User.findOne({ _id: userId })
  .exec()
  .then(user => {
    if (!user) {
      return res.status(401).json({
        message: "User not exists!"
      });
    }else{
      // procurar mensagens de todos os amigos
      user.friends.push(user._id);
      var isMyFriend = false;
     
      for (var i = 0; i < user.friends.length; i++) { 
        if(user.friends[i]._id.toString()  == friendId.toString() ){
          isMyFriend = true;
        }
      }

      if(friendId == userId){
        console.log("É o autenticado!");
        Message.find({ postedBy: userId  })
        .sort('-date')
        .populate('postedBy','-password')
        .exec()
        .then(messages => {
          if (!messages) {
            return res.status(401).json({
              message: "Messages not exists!"
            });
          }else{
            // procurar todos os amigos
            //console.log(messages);
            return res.status(200).json({
              message: "Messages returned succesfully",
              messages: messages
            });
            
          }
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
      }

      if(isMyFriend){
        console.log("É amigo!");
        Message.find({ postedBy: friendId  })
        .sort('-date')
        .populate('postedBy','-password')
        .exec()
        .then(messages => {
          if (!messages) {
            return res.status(401).json({
              message: "Messages not exists!"
            });
          }else{
            // procurar todos os amigos
          //  console.log(messages);
            return res.status(200).json({
              message: "Messages returned succesfully",
              messages: messages
            });
            
          }
        })
        .catch(err => {
          console.log(err);
          res.status(500).json({
            error: err
          });
        });
      }

    }
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
      error: err
    });
  }); 
};