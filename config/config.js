'use strict'

module.exports = {
    server_url: 'http://localhost:80/',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 80,
    db: {
        uri: 'mongodb://35.233.5.118:27017/cn_proj',
    },
    jwt_key: process.env.JWT_KEY || "cnproj3"
}

