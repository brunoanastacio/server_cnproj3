const jwt = require('jsonwebtoken');
const config  = require('../../config/config.js');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, config.jwt_key);
        req.userData = decoded;
        console.log(req.userData);
        console.log("CHECK-AUTH");
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'Auth failed, maybe token expired'
        });
    }
};