'use strict';
const User = require("../models/user");
const Message = require("../models/message");

const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
var faker = require("faker");
const publicIp = require('public-ip');
const getIP = require('external-ip')();


const numberOfUsers = 20000;
var userIds = [];

exports.createUsers = (req, res, next) => {
  console.log("Seeding users...");
  var username = "";
  var password = "";
  var name = "";
  var img_photo = "";
  var user;

  for(var i = 0; i < numberOfUsers; i++){
    username = "user" + i;
    password = '$2a$10$uyn.SsyPAD4e/xDYwob/XuyuNciZplx4lF/raZxUyIK/pIUKvaQa6';
    name = username;
    img_photo = faker.image.avatar();  

    user = new User({
      _id: new mongoose.Types.ObjectId(),
      username: username,
      password: password,
      name: name,
      img_photo: img_photo
    });
    user
    .save()
    .then(result => {
      //console.log(result);
    }).catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
  } 
  res.status(200).json({
    message: "Successfully"
  });
  console.log("End seeding users!");
};

//devem ter 100 amigos random e 3 amigos dos 10 primeiros
exports.friendsFor3Firsts = (req, res, next) => {
  console.log("Seeding friends for 3 first users...");
      //user 1 
      User.findOne({ username: "user0" })
      .exec()
      .then(user => {
        if (!user) {
          console.log("User not exists!");
        }else{
          user.friends.push(userIds[1]);
          user.friends.push(userIds[2]);
          user.friends.push(userIds[3]);
  
          var random;
          for(var x = 0; x < 100; x++ ){
            random = Math.floor(Math.random() * Math.floor(numberOfUsers));
            if(random > 3){
              user.friends.push(userIds[random]);
            }else{
              random = Math.floor(Math.random() * Math.floor(numberOfUsers));
              if(random > 3){
                user.friends.push(userIds[random]);
              }
            }
          }
  
          user
          .save()
          .then(result => {
            //console.log(result);

          }).catch(err => {
            console.log(err);
          });
        }
     })
  
     //user 2 
     User.findOne({ username: "user1" })
     .exec()
     .then(user => {
       if (!user) {
         console.log("User not exists!");
       }else{
         user.friends.push(userIds[2]);
         user.friends.push(userIds[3]);
         user.friends.push(userIds[4]);
  
         var random;
         for(var x = 0; x < 100; x++ ){
           random = Math.floor(Math.random() * Math.floor(numberOfUsers));
           if(random > 4){
             user.friends.push(userIds[random]);
           }else{
            random = Math.floor(Math.random() * Math.floor(numberOfUsers));
            if(random > 4){
              user.friends.push(userIds[random]);
            }
          }
         }
  
         user
         .save()
         .then(result => {
           //console.log(result);
         }).catch(err => {
           console.log(err);
         });
       }
    })
  
    //user 3
    User.findOne({ username: "user2" })
    .exec()
    .then(user => {
      if (!user) {
        console.log("User not exists!");
      }else{
        user.friends.push(userIds[3]);
        user.friends.push(userIds[4]);
        user.friends.push(userIds[5]);
  
        var random;
        for(var x = 0; x < 100; x++ ){
          random = Math.floor(Math.random() * Math.floor(numberOfUsers));
          if(random > 5){
            user.friends.push(userIds[random]);
          }else{
            random = Math.floor(Math.random() * Math.floor(numberOfUsers));
            if(random > 5){
              user.friends.push(userIds[random]);
            }
          }
        }
  
        user
        .save()
        .then(result => {
          //console.log(result);
        }).catch(err => {
          console.log(err);
        });
      }
   })
  res.status(200).json({
    message: "Successfully"
  });
  console.log("End seeding friends for 3 first users!");
};

exports.saveUserIdsInArray =(req, res, next) => {
  userIds = [];
  User.find({}).exec(
    function(error, users) { 
    for(var a = 0; a < users.length; a++){
      userIds.push(users[a]._id);
    }
    res.status(200).json({
      message: "ID's: " + userIds.length
    });
  });
};

exports.getRandomInt =(req, res, next)  => {
  return Math.floor(Math.random() * Math.floor(numberOfUsers));
};

//devem ter 100 amigos random 
exports.friendsForAll = (req, res, next) => {
  console.log("Seeding friends for all users...");
  console.log(userIds.length);

    for(var i = 3; i < userIds.length; i++){
      User.findOne({ _id : userIds[i] })
      .exec()
      .then(user => {
        if (!user) {
          console.log("User not exists!");
        }else{
          var random;
          for(var x = 0; x < 10; x++ ){
            random = Math.floor(Math.random() * Math.floor(numberOfUsers));
            if(random != i){
              user.friends.push(userIds[random]);
            }else{
              random = Math.floor(Math.random() * Math.floor(numberOfUsers));
              if(random != i){
                user.friends.push(userIds[random]);
              }
            }
          }

          user
          .save()
          .then(result => {
            console.log(result);

          }).catch(err => {
            console.log(err);
          });
        }
      })

    }
    res.status(200).json({
      message: "Successfully"
    });

  console.log("End seeding friends for all users!");
};

exports.create_messages = (req, res, next) => {
  console.log("Seeding messages for all users...");
  console.log(userIds.length);
  var msg; var name = ""; var id ; var img ; var random;
    for(var i = 0; i < userIds.length; i++){
      User.findOne({ _id: userIds[i] })
      .exec()
      .then(user => {
        if (!user) {
          console.log("User not exists!");
        }else{
          name = user.name;
          id = user._id;
         
          for(var x = 0; x < 2; x++){
            random = Math.random();
            if(random <= 0.2){
              img = faker.image.avatar();
            }else{
              img ="";
            }
  
            //console.log(i);
            msg = new Message({
              _id: new mongoose.Types.ObjectId(),
              postedBy: id,
              img_photo: img,
              message: faker.lorem.sentence(),
              name: name
            });
      
            msg.save()
              .then(result => {
                //console.log(result);
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
            });
          } 
        }
      })
    }

    console.log("Finish!");

    res.status(200).json({
      message: "Successfully"
    });

  console.log("End seeding messages for all users!");
};

exports.get_ip =(req, res, next) => {
  getIP((err, ip) => {
    if (err) {
        // every service in the list has failed
        throw err;
    }else{
      res.status(200).json({
        ip: ip
      });
    }
    console.log(ip);
  });
 

 
};
