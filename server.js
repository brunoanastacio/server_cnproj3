const config  = require('./config/config.js');
const http = require('http');
const app = require('./app');

const port = config.port;

const server = http.createServer(app);

server.listen(port);