const express = require("express");
const router = express.Router();

const UserController = require('../controllers/user');
const MessageController = require('../controllers/message');
const checkAuth = require('../middleware/check-auth');
const configAWS  = require('../../config/config_dynamo.json');
var AWS = require('aws-sdk');
var path = require('path')
const uuidv1 = require('uuid/v1');
var multer  = require('multer'), multerS3 = require('multer-s3');

var s3 = new AWS.S3({
    accessKeyId: configAWS.accessKeyId,
    secretAccessKey: configAWS.secretAccessKey,
    region: configAWS.region
  });

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
};

//file.originalname
const uploadMessagePhoto = multer({
    storage: multerS3({
          s3: s3,
          bucket: 'cnproj3',
          acl: 'public-read',
          metadata: function (req, file, cb) {
              cb(null, { fieldName: file.fieldname });
              if(file.originalname != undefined ){
                cb(null, { fieldName: file.fieldname });
              }
          },
          key: function (req, file, cb) {
              if(file.originalname != undefined ){
                var ext = path.extname(file.originalname);
                var random = uuidv1();
                cb(null, req.userData.userId+"+"+random+ext); //use Date.now() for unique file keys
              }
          }
      }),
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });


router.get("/searchUser/:friendId",checkAuth, MessageController.searchUserMessages);
router.post("/create",checkAuth ,uploadMessagePhoto.single('msgimg'), MessageController.createMsg);
router.get("/lastmsg",checkAuth , MessageController.lastmsg);
router.get("/search/:usernameFilter?/:messageFilter?",checkAuth , MessageController.search);


module.exports = router;
