const mongoose = require('mongoose');

const messageSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    message: { type: String, required: true },
    img_photo: { type: String, default: "" },
    postedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    date: { type: Date, default: Date.now },
    name: { type: String, required: true }
});

module.exports = mongoose.model('Message', messageSchema);