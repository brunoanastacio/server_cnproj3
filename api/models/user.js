const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    img_photo:  { type: String},
    friend_requests: [{_id:{ type: mongoose.Schema.Types.ObjectId,  ref: 'User'}}],
    friends: [{_id:{ type: mongoose.Schema.Types.ObjectId, ref: 'User'}}]
});

userSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('User', userSchema);